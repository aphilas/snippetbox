include .env
export

db:
	docker compose up

run:
	go run ./cmd/web -dsn=postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME} -addr=:4000
