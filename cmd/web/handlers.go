package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/aphilas/snippetbox/internal/models"
	"gitlab.com/aphilas/snippetbox/internal/validator"
)

const (
	sessKeyFlash    = "flash"
	sessKeyUserID   = "authenticatedUserID"
	sessKeyRedirect = "redirect"
)

type snippetCreateForm struct {
	Title               string `form:"title"`
	Content             string `form:"content"`
	Expires             int    `form:"expires"`
	validator.Validator `form:"-"`
}

type userSignupForm struct {
	Name                string `form:"name"`
	Email               string `form:"email"`
	Password            string `form:"password"`
	validator.Validator `form:"-"`
}

type userLoginForm struct {
	Email               string `form:"email"`
	Password            string `form:"password"`
	validator.Validator `form:"-"`
}

type passwordUpdateForm struct {
	CurrentPassword      string `form:"currentPassword"`
	NewPassword          string `form:"newPassword"`
	PasswordConfirmation string `form:"passwordConfirmation"`
	validator.Validator  `form:"-"`
}

func ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	snippets, err := app.snippets.Latest()
	if err != nil {
		app.serverError(w, err)
		return
	}

	data := app.newTemplateData(r)
	data.Snippets = snippets

	app.render(w, http.StatusOK, "home.tmpl", data)
}

// GET /snippets/{id}
func (app *application) snippetView(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.PathValue("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	snippet, err := app.snippets.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
		return
	}

	data := app.newTemplateData(r)
	data.Snippet = snippet

	app.render(w, http.StatusOK, "view.tmpl", data)
}

// GET /snippets/new
func (app *application) snippetNew(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)

	data.Form = snippetCreateForm{
		Expires: 365,
	}

	app.render(w, http.StatusOK, "create.tmpl", data)
}

// POST /snippets
func (app *application) snippetCreate(w http.ResponseWriter, r *http.Request) {
	// Adds data in POST request bodies to r.PostForm
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	expires, err := strconv.Atoi(r.PostForm.Get("expires"))
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := snippetCreateForm{
		Title:   r.PostForm.Get("title"),
		Content: r.PostForm.Get("content"),
		Expires: expires,
	}

	form.CheckField(validator.NotBlank(form.Title), "title", "This field cannot be blank")
	form.CheckField(validator.CharLength(form.Title, 1, 100), "title", "This field cannot be more than 100 characters long")
	form.CheckField(validator.NotBlank(form.Content), "content", "This field cannot be blank")
	form.CheckField(validator.CharLength(form.Content, 1, 1000), "title", "This field cannot be more than 1000 characters long")
	form.CheckField(validator.PermittedValue(form.Expires, 1, 7, 365), "expires", "This field must equal 1, 7 or 365")

	// IF there are validation errors redisplay the form and pass the old values
	// and validation errors.
	if !form.Valid() {
		data := app.newTemplateData(r)
		data.Form = form
		app.render(w, http.StatusUnprocessableEntity, "create.tmpl", data)
		return
	}

	id, err := app.snippets.Insert(form.Title, form.Content, form.Expires)
	if err != nil {
		app.serverError(w, err)
		return
	}

	sess := app.sessionManager.Get(r)
	sess.Set(sessKeyFlash, "Snippet created successfully!")

	http.Redirect(w, r, fmt.Sprintf("/snippets/%d", id), http.StatusSeeOther)
}

// GET /user/signup
func (app *application) userSignup(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	data.Form = userSignupForm{}
	app.render(w, http.StatusOK, "signup.tmpl", data)
}

// POST /user/signup
func (app *application) userSignupPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := userSignupForm{
		Name:     r.PostFormValue("name"),
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
	}

	form.CheckField(validator.NotBlank(form.Name), "name", "This field cannot be blank")
	form.CheckField(validator.NotBlank(form.Email), "email", "This field cannot be blank")
	form.CheckField(validator.Matches(form.Email, validator.EmailRe), "email", "This field must be a valid email address")
	form.CheckField(validator.NotBlank(form.Password), "password", "This field cannot be blank")

	// TODO: Validate password complexity
	form.CheckField(validator.CharLength(form.Password, 8, 128), "password", "This field must be between 8 and 128 characters long")

	if !form.Valid() {
		data := app.newTemplateData(r)
		data.Form = form
		app.render(w, http.StatusUnprocessableEntity, "signup.tmpl", data)
		return
	}

	err = app.users.Insert(form.Name, form.Email, form.Password)
	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			form.AddFieldError("email", "Email address is already in use")

			data := app.newTemplateData(r)
			data.Form = form
			app.render(w, http.StatusUnprocessableEntity, "signup.tmpl", data)
		} else {
			app.serverError(w, err)
		}

		return
	}

	sess := app.sessionManager.Get(r)
	sess.Set(sessKeyFlash, "Your signup was successful. Please log in.")

	http.Redirect(w, r, "/user/login", http.StatusSeeOther)
}

// GET /user/login
func (app *application) userLogin(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	data.Form = userLoginForm{}
	app.render(w, http.StatusOK, "login.tmpl", data)
}

// POST /user/login
func (app *application) userLoginPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := userLoginForm{
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
	}

	form.CheckField(validator.NotBlank(form.Email), "email", "This field cannot be blank")
	form.CheckField(validator.Matches(form.Email, validator.EmailRe), "email", "This field must be a valid email address")
	form.CheckField(validator.NotBlank(form.Password), "password", "This field cannot be blank")

	if !form.Valid() {
		data := app.newTemplateData(r)
		data.Form = form
		app.render(w, http.StatusUnprocessableEntity, "login.tmpl", data)
		return
	}

	id, err := app.users.Authenticate(form.Email, form.Password)
	if err != nil {
		if errors.Is(err, models.ErrInvalidCredentials) {
			form.AddNonFieldError("Email or password is incorrect")
			data := app.newTemplateData(r)
			data.Form = form
			app.render(w, http.StatusUnprocessableEntity, "login.tmpl", data)
		} else {
			app.serverError(w, err)
		}
		return
	}

	// TODO: Renew token
	// err = app.sessionManager.RenewToken(r.Context())
	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }
	// Add the ID of the current user to the session, so that they are now
	// 'logged in'.

	app.sessionManager.Get(r).Set(sessKeyUserID, id)
	redirect := app.sessionManager.Get(r).PopString(sessKeyRedirect)
	if redirect != "" {
		http.Redirect(w, r, redirect, http.StatusSeeOther)
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// POST /user/logout
func (app *application) userLogoutPost(w http.ResponseWriter, r *http.Request) {
	// TODO: Renew token
	// err := app.sessionManager.RenewToken(r.Context())
	// if err != nil {
	// 	app.serverError(w, err)
	// 	return
	// }

	app.sessionManager.Get(r).Delete(sessKeyUserID)

	app.sessionManager.Get(r).Set(sessKeyFlash, "You've been logged out successfully!")

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// GET /about
func (app *application) about(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	app.render(w, http.StatusOK, "about.tmpl", data)
}

// GET /account
func (app *application) account(w http.ResponseWriter, r *http.Request) {
	id := app.sessionManager.Get(r).GetInt(sessKeyUserID)

	user, err := app.users.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			http.Redirect(w, r, "/user/login", http.StatusSeeOther)
		} else {
			app.serverError(w, err)
		}
		return
	}

	data := app.newTemplateData(r)
	data.User = user

	app.render(w, http.StatusOK, "account.tmpl", data)
}

// GET /account/change-password
func (app *application) updatePassword(w http.ResponseWriter, r *http.Request) {
	data := app.newTemplateData(r)
	data.Form = passwordUpdateForm{}
	app.render(w, http.StatusOK, "password.tmpl", data)
}

// POST /account/change-password
func (app *application) updatePasswordPost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	form := passwordUpdateForm{
		CurrentPassword:      r.PostFormValue("currentPassword"),
		NewPassword:          r.PostFormValue("newPassword"),
		PasswordConfirmation: r.PostFormValue("passwordConfirmation"),
	}

	form.CheckField(validator.NotBlank(form.CurrentPassword), "currentPassword", "This field cannot be blank")
	form.CheckField(validator.NotBlank(form.NewPassword), "newPassword", "This field cannot be blank")
	form.CheckField(validator.NotBlank(form.PasswordConfirmation), "passwordConfirmation", "This field cannot be blank")

	// TODO: Validate password complexity
	form.CheckField(validator.CharLength(form.NewPassword, 8, 128), "newPassword", "This field must be between 8 and 128 characters long")
	form.CheckField(validator.Equal(form.NewPassword, form.PasswordConfirmation), "passwordConfirmation", "Passwords do not match")

	if !form.Valid() {
		data := app.newTemplateData(r)
		data.Form = form
		app.render(w, http.StatusUnprocessableEntity, "password.tmpl", data)
		return
	}

	id := app.sessionManager.Get(r).GetInt(sessKeyUserID)

	err = app.users.UpdatePassword(id, form.CurrentPassword, form.NewPassword)
	if err != nil {
		if errors.Is(err, models.ErrInvalidCredentials) {
			form.AddFieldError("currentPassword", "Password invalid")

			data := app.newTemplateData(r)
			data.Form = form
			app.render(w, http.StatusUnprocessableEntity, "password.tmpl", data)
		} else {
			app.serverError(w, err)
		}

		return
	}

	sess := app.sessionManager.Get(r)
	sess.Set(sessKeyFlash, "Password update successful. Please log in.")

	// TODO: log out user?

	http.Redirect(w, r, "/user/login", http.StatusSeeOther)
}
