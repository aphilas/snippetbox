package main

import (
	"fmt"
	"html/template"
	"io/fs"
	"net/http"
	"path/filepath"
	"time"

	"gitlab.com/aphilas/snippetbox/internal/models"
	"gitlab.com/aphilas/snippetbox/web"
)

type templateData struct {
	CurrentYear     int
	User            *models.User
	Snippet         *models.Snippet
	Snippets        []*models.Snippet
	Form            any
	Flash           string
	IsAuthenticated bool
}

func humanDate(t time.Time) string {
	if t.IsZero() {
		return ""
	}

	// We convert the time to UTC before formatting it
	return t.UTC().Format("02 Jan 2006 at 15:04")
}

var functions = template.FuncMap{
	"humanDate": humanDate,
}

func newTemplateCache() (map[string]*template.Template, error) {
	cache := map[string]*template.Template{}

	pages, err := fs.Glob(web.Files, "html/pages/*.tmpl")
	if err != nil {
		return nil, fmt.Errorf("generating glob files: %w", err)
	}

	for _, page := range pages {
		name := filepath.Base(page)

		patterns := []string{"html/base.tmpl", "html/partials/*.tmpl", page}

		// Parse the base template file into a template set.
		ts, err := template.New(name).Funcs(functions).ParseFS(web.Files, patterns...)
		if err != nil {
			return nil, err
		}

		cache[name] = ts
	}

	return cache, nil
}

func (app *application) newTemplateData(r *http.Request) *templateData {
	sess := app.sessionManager.Get(r)

	return &templateData{
		CurrentYear:     time.Now().Year(),
		Flash:           sess.PopString(sessKeyFlash),
		IsAuthenticated: app.isAuthenticated(r),
	}
}
