package main

import (
	"bytes"
	"html"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"regexp"
	"testing"

	"gitlab.com/aphilas/snippetbox/internal/models/mocks"
	"gitlab.com/aphilas/snippetbox/internal/session"
)

var csrfTokenRe = regexp.MustCompile(`<input type='hidden' name='csrf_token' value='(.+)'>`)

// newTestApplication returns an instance of our application struct containing
// mocked dependencies.
func newTestApplication(t *testing.T) *application {
	templateCache, err := newTemplateCache()
	if err != nil {
		t.Fatal(err)
	}

	// If were were using a persistent store, this would be the best place to
	// switch to an in-memory store.
	sessionManager := session.New()
	sessionManager.CookieConfig.Secure = true

	return &application{
		errorLog:       log.New(io.Discard, "", 0),
		infoLog:        log.New(io.Discard, "", 0),
		snippets:       &mocks.SnippetModel{},
		users:          &mocks.UserModel{},
		templateCache:  templateCache,
		sessionManager: sessionManager,
	}
}

// testServer defines a custom testServer type which embeds a httptest.Server
// instance.
type testServer struct {
	*httptest.Server
}

// newTestServer defines a testServer that adds a cookie jar, and disables redirects
func newTestServer(t *testing.T, h http.Handler) *testServer {
	ts := httptest.NewTLSServer(h)

	jar, err := cookiejar.New(nil)
	if err != nil {
		t.Fatal(err)
	}

	// any response cookies will now be stored and sent with subsequent requests
	// when using this client.
	ts.Client().Jar = jar

	// Disable redirect-following. This function will be called whenever a 3xx
	// response is received by the client. By always returning a
	// http.ErrUseLastResponse error it forces the client to immediately return
	// the received response.
	ts.Client().CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	return &testServer{ts}
}

// get makes a GET request to a given url path using the test server client, and
// returns the response status code, headers and body.
func (ts *testServer) get(t *testing.T, urlPath string) (statusCode int, headers http.Header, body string) {
	// The network address is contained in the ts.URL field.
	rs, err := ts.Client().Get(ts.URL + urlPath)
	if err != nil {
		t.Fatal(err)
	}

	defer rs.Body.Close()
	buf, err := io.ReadAll(rs.Body)
	if err != nil {
		t.Fatal(err)
	}

	return rs.StatusCode, rs.Header, string(bytes.TrimSpace(buf))
}

// postForm sends POST requests to the test server. The final parameter to this
// method is a url.Values object which can contain any form data that you want
// to send in the request body.
func (ts *testServer) postForm(t *testing.T, urlPath string, form url.Values) (int, http.Header, string) {
	rs, err := ts.Client().PostForm(ts.URL+urlPath, form)
	if err != nil {
		t.Fatal(err)
	}
	defer rs.Body.Close()

	body, err := io.ReadAll(rs.Body)
	if err != nil {
		t.Fatal(err)
	}

	return rs.StatusCode, rs.Header, string(bytes.TrimSpace(body))
}

func extractCSRFToken(t *testing.T, body string) string {
	matches := csrfTokenRe.FindStringSubmatch(body)
	if len(matches) < 2 {
		t.Fatal("no csrf token found in body")
	}

	return html.UnescapeString(string(matches[1]))
}
