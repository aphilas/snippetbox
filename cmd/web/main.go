package main

//go:generate npm run build

import (
	"crypto/tls"
	"database/sql"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib"
	"gitlab.com/aphilas/snippetbox/internal/models"
	"gitlab.com/aphilas/snippetbox/internal/session"
)

const (
	certFile = "./tls/cert.pem"
	keyFile  = "./tls/key.pem"

	// Timeout for keep-alive connections.
	// If not set, the read timeout will be used.
	idleTimeout = time.Minute

	// Hard limit on request heads (returns no response if exceeded).
	readTimeout = 5 * time.Second

	writeTimeout = 10 * time.Second

	// Default is 1MB
	maxHeaderBytes = 512 * 1024
)

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger

	// We use interfaces to allow us to swap out implementations for mocks.
	snippets models.SnippetModeler
	users    models.UserModeler

	templateCache  map[string]*template.Template
	sessionManager *session.Manager

	// Whether or not to start the application in debug mode.
	debug bool
}

func main() {
	addr := flag.String("addr", ":4000", "HTTP network address")
	dsn := flag.String("dsn", "", "DB data source name")
	debug := flag.Bool("debug", false, "Display detailed errors and stack traces")

	// Parse command line into defined flags after flags are defined
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	// Validate command line flags
	if *dsn == "" {
		errorLog.Fatal("dsn required")
	}

	db, err := openDB(*dsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer db.Close()

	templateCache, err := newTemplateCache()
	if err != nil {
		errorLog.Fatal(err)
	}

	sessionManager := session.New()
	sessionManager.CookieConfig.Secure = true

	app := &application{
		errorLog:       errorLog,
		infoLog:        infoLog,
		snippets:       &models.SnippetModel{DB: db},
		users:          &models.UserModel{DB: db},
		templateCache:  templateCache,
		sessionManager: sessionManager,
		debug:          *debug,
	}

	tlsConfig := &tls.Config{
		// Using TLS ≥ 1.3, and Same-Site: Lax or Same-Site: Strict
		// we can avoid CSRF attacks without using a CSRF token
		MinVersion: tls.VersionTLS13,
	}

	srv := &http.Server{
		TLSConfig:      tlsConfig,
		Addr:           *addr,
		ErrorLog:       errorLog,
		Handler:        app.routes(),
		IdleTimeout:    idleTimeout,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	infoLog.Printf("Starting server on %s\n", *addr)

	err = srv.ListenAndServeTLS(certFile, keyFile)
	errorLog.Fatal(err)
}

func openDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, fmt.Errorf("opening db: %w", err)
	}

	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("pinging db: %w", err)
	}

	return db, nil
}
