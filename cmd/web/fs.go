package main

import (
	"net/http"
	"os"
	"path/filepath"
)

type NoListFileSystem struct {
	base http.FileSystem
}

func (fs NoListFileSystem) Open(name string) (http.File, error) {
	f, err := fs.base.Open(name)
	if err != nil {
		return nil, err
	}

	info, err := f.Stat()
	if err != nil {
		return nil, err
	}
	if info.IsDir() {
		index := filepath.Join(name, "index.html")
		if _, err := fs.base.Open(index); err != nil {
			if err = f.Close(); err != nil { // close original file - ?
				return nil, err
			}

			return nil, os.ErrNotExist // dir has no index.html
		}
	}

	return NoListFile{f}, nil
}

type NoListFile struct {
	http.File
}

func (f NoListFile) Readdir(count int) ([]os.FileInfo, error) {
	return nil, nil
}
