package main

import (
	"net/http"

	"gitlab.com/aphilas/snippetbox/web"
)

func (app *application) routes() http.Handler {
	mux := http.NewServeMux()

	// Convert embed.FS to http.FileSystem
	fs := NoListFileSystem{http.FS(web.Files)}

	fileServer := http.FileServer(fs)

	mux.Handle("GET /static/", fileServer)

	mux.Handle("GET /ping", http.HandlerFunc(ping))

	// Middleware chains
	standard := []Middleware{app.recoverPanic, app.logRequest, secureHeaders}

	dynamic := []Middleware{app.sessionManager.LoadSession, app.authenticate}
	protected := []Middleware{app.sessionManager.LoadSession, app.authenticate, app.requireAuthentication}

	mux.Handle("/{$}", ChainFunc(app.home, dynamic...))
	mux.Handle("/about", ChainFunc(app.about, dynamic...))
	mux.Handle("GET /snippets/{id}", ChainFunc(app.snippetView, dynamic...))

	mux.Handle("GET /user/signup", ChainFunc(app.userSignup, dynamic...))
	mux.Handle("POST /user/signup", ChainFunc(app.userSignupPost, dynamic...))
	mux.Handle("GET /user/login", ChainFunc(app.userLogin, dynamic...))
	mux.Handle("POST /user/login", ChainFunc(app.userLoginPost, dynamic...))

	mux.Handle("POST /user/logout", ChainFunc(app.userLogoutPost, protected...))
	mux.Handle("GET /account", ChainFunc(app.account, protected...))
	mux.Handle("GET /account/change-password", ChainFunc(app.updatePassword, protected...))
	mux.Handle("POST /account/change-password", ChainFunc(app.updatePasswordPost, protected...))

	mux.Handle("GET /snippets/new", ChainFunc(app.snippetNew, protected...))
	mux.Handle("POST /snippets", ChainFunc(app.snippetCreate, protected...))

	return Chain(mux, standard...)
}
