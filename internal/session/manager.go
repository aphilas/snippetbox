package session

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"net/http"
	"time"
)

type ctxKey struct{}

type Manager struct {
	IdleTimeout     time.Duration
	AbsoluteTimeout time.Duration
	CtxKey          ctxKey
	CookieConfig    CookieConfig
	Store           *MemStore
}

type CookieConfig struct {
	Name     string
	Domain   string
	HttpOnly bool
	Path     string
	Persist  bool
	SameSite http.SameSite
	Secure   bool
}

func New() *Manager {
	s := &Manager{
		AbsoluteTimeout: 24 * time.Hour,
		Store:           NewMemStore(),
		CtxKey:          ctxKey{},
		CookieConfig: CookieConfig{
			Name:     "session",
			Domain:   "",
			HttpOnly: true,
			Path:     "/",
			Persist:  true,
			Secure:   false,
			SameSite: http.SameSiteLaxMode,
		},
	}
	return s
}

// LoadSession gets the session from the cookie and loads it into http.Request context.
func (m *Manager) LoadSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sess := m.GetCookieSession(r)

		if sess == nil {
			sess = NewSession(time.Now().Add(m.AbsoluteTimeout), 0)
			m.Add(w, r, sess)
		}

		ctx := context.WithValue(r.Context(), m.CtxKey, sess)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// GetCookieSession reads the session value from the request cookies.
func (m *Manager) GetCookieSession(r *http.Request) *Session {
	cookie, err := r.Cookie(m.CookieConfig.Name)
	if err != nil {
		return nil
	}

	return m.Store.Get(cookie.Value)
}

// Get retrieves the session from the request context.
func (m *Manager) Get(r *http.Request) *Session {
	sess, ok := r.Context().Value(m.CtxKey).(*Session)
	if ok {
		return sess
	}

	return nil
}

// Add saves a session to the store and persists it with a cookie.
func (m *Manager) Add(w http.ResponseWriter, r *http.Request, sess *Session) {
	m.Store.Add(sess)
	m.WriteCookie(w, sess.ID, time.Now().Add(m.AbsoluteTimeout))
}

// Delete removes the session from the store.
func (m *Manager) Delete(r *http.Request) {
	cookie, err := r.Cookie(m.CookieConfig.Name)
	if err != nil {
		return
	}

	m.Store.Delete(cookie.Value)
	// TODO: Delete session cookie
}

// // Refresh refreshes the token while retaining it's data
// func (m *Manager) Refresh(r *http.Request) error {
// 	oldSess, ok := r.Context().Value(m.CtxKey).(*Session)
// 	if !ok {
// 		return fmt.Errorf("session not found")
// 	}

// 	sess := NewSession(time.Now().Add(m.AbsoluteTimeout), 0)

// 	m.Store.Delete(oldSess.ID)

// 	return nil
// }

func (m *Manager) WriteCookie(w http.ResponseWriter, token string, expiry time.Time) {
	cookie := &http.Cookie{
		Name:     m.CookieConfig.Name,
		Value:    token,
		Path:     m.CookieConfig.Path,
		Domain:   m.CookieConfig.Domain,
		Secure:   m.CookieConfig.Secure,
		HttpOnly: m.CookieConfig.HttpOnly,
		SameSite: m.CookieConfig.SameSite,
	}

	if expiry.IsZero() {
		// Delete cookie
		cookie.Expires = time.Unix(1, 0)
		cookie.MaxAge = -1
	} else if m.CookieConfig.Persist {
		cookie.Expires = expiry
		cookie.MaxAge = int(time.Until(expiry).Seconds())
	}

	w.Header().Add("Set-Cookie", cookie.String())
	w.Header().Add("Cache-Control", `no-cache="Set-Cookie"`)
}

// genID generates a random base64 encoded string.
func genID(entropyBytes int) string {
	b := make([]byte, entropyBytes)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}
