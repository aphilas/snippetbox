package session

import (
	"sync"
	"time"
)

const idEntropy = 33

type Session struct {
	ID       string
	Created  time.Time
	Accessed time.Time
	Expiry   time.Time
	// Used to handle idle timeout
	Timeout time.Duration
	Attrs   map[string]any
	sync.RWMutex
}

func (s *Session) Get(name string) any {
	s.Lock()
	defer s.Unlock()
	s.Accessed = time.Now()
	return s.Attrs[name]
}

func (s *Session) GetString(name string) string {
	v, ok := s.Get(name).(string)
	if !ok {
		return ""
	}
	return v
}

func (s *Session) GetInt(name string) int {
	v, ok := s.Get(name).(int)
	if !ok {
		return 0
	}
	return v
}

func (s *Session) GetBool(name string) bool {
	v, ok := s.Get(name).(bool)
	if !ok {
		return false
	}
	return v
}

func (s *Session) Pop(name string) any {
	s.Lock()
	defer s.Unlock()

	s.Accessed = time.Now()
	v := s.Attrs[name]
	delete(s.Attrs, name)

	return v
}

func (s *Session) PopString(name string) string {
	v, ok := s.Pop(name).(string)
	if !ok {
		return ""
	}
	return v
}

func (s *Session) Set(name string, value any) {
	s.Lock()
	defer s.Unlock()
	s.Accessed = time.Now()
	s.Attrs[name] = value
}

func (s *Session) Delete(name string) {
	s.Lock()
	defer s.Unlock()
	s.Accessed = time.Now()
	delete(s.Attrs, name)
}

func NewSession(expiry time.Time, timeout time.Duration) *Session {
	return &Session{
		ID:       genID(idEntropy),
		Timeout:  timeout,
		Created:  time.Now(),
		Accessed: time.Now(),
		Expiry:   expiry,
		Attrs:    make(map[string]any),
	}
}
