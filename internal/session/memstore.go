package session

import (
	"sync"
	"time"
)

const cleanUpInterval = time.Minute

// MemStore is an in-memory session storage implementation
type MemStore struct {
	sessions map[string]*Session
	sync.RWMutex
}

func NewMemStore() *MemStore {
	m := &MemStore{
		sessions: make(map[string]*Session),
	}

	go m.start(cleanUpInterval)

	return m
}

func (m *MemStore) start(interval time.Duration) {
	ticker := time.NewTicker(interval)
	for range ticker.C {
		m.deleteExpired()
	}
}

func (m *MemStore) Get(id string) *Session {
	m.RLock()
	defer m.RUnlock()

	sess, ok := m.sessions[id]
	if !ok {
		return nil
	}

	if time.Now().After(sess.Expiry) {
		return nil
	}

	return sess
}

func (m *MemStore) Add(sess *Session) {
	m.Lock()
	m.sessions[sess.ID] = sess
	m.Unlock()
}

func (m *MemStore) Delete(id string) {
	m.RLock()
	defer m.RUnlock()

	delete(m.sessions, id)
}

func (m *MemStore) deleteExpired() {
	now := time.Now()
	for id, sess := range m.sessions {
		if now.After(sess.Expiry) ||
			(sess.Timeout != 0 && now.Sub(sess.Accessed) > sess.Timeout) {
			delete(m.sessions, id)
		}
	}
}
