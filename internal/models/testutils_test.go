package models

import (
	"database/sql"
	"os"
	"testing"

	_ "github.com/jackc/pgx/v5/stdlib"
)

const (
	ddlPath      = "../../migrations/ddl.sql"
	teardownPath = "../../migrations/teardown.sql"
	testSeedPath = "./testdata/seed.sql"
)

// newTestDB initializes the schema for the test db, inserts seed data, and then
// drops the tables at the end of the test/sub-test.
func newTestDB(t *testing.T) *sql.DB {
	db, err := sql.Open("pgx", "postgres://test_web:pass@localhost:5433/test_snippetbox")
	if err != nil {
		t.Fatalf("opening db: %v", err)
	}

	runScript := func(path string) {
		script, err := os.ReadFile(path)
		if err != nil {
			t.Fatalf("opening %s: %v", path, err)
		}

		_, err = db.Exec(string(script))
		if err != nil {
			t.Fatalf("running %s: %v", path, err)
		}
	}

	runScript(ddlPath)
	runScript(testSeedPath)

	// Register a function which will automatically be called when the current
	// test (or sub-test) has finished. In this function we read and execute the
	// teardown script, and close the database connection pool.
	t.Cleanup(func() {
		runScript(teardownPath)
		db.Close()
	})

	return db
}
