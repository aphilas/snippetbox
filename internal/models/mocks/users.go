package mocks

import (
	"time"

	"gitlab.com/aphilas/snippetbox/internal/models"
)

type UserModel struct{}

const (
	mockUserID         = 1
	mockUserEmail      = "alice@example.com"
	mockUserPassword   = "pa$$word"
	mockDuplicateEmail = "dupe@example.com"
)

func (m *UserModel) Insert(name, email, password string) error {
	switch email {
	case mockDuplicateEmail:
		return models.ErrDuplicateEmail
	default:
		return nil
	}
}

func (m *UserModel) Authenticate(email, password string) (int, error) {
	if email == mockUserEmail && password == mockUserPassword {
		return mockUserID, nil
	}
	return 0, models.ErrInvalidCredentials
}

func (m *UserModel) Exists(id int) (bool, error) {
	switch id {
	case mockUserID:
		return true, nil
	default:
		return false, nil
	}
}

func (m *UserModel) Get(id int) (*models.User, error) {
	switch id {
	case mockUserID:
		u := &models.User{
			ID:      mockUserID,
			Name:    "Alice",
			Email:   mockUserEmail,
			Created: time.Now(),
		}

		return u, nil
	default:
		return nil, models.ErrNoRecord
	}
}

func (m *UserModel) UpdatePassword(id int, currentPassword, newPassword string) error {
	if id == mockUserID {
		if currentPassword == mockUserPassword {
			return nil
		}

		return models.ErrInvalidCredentials
	}

	return models.ErrNoRecord
}
