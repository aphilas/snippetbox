-- creates the test db and user

CREATE DATABASE test_snippetbox;

CREATE USER test_web WITH PASSWORD 'pass';

GRANT ALL PRIVILEGES ON DATABASE test_snippetbox TO test_web;

ALTER DATABASE test_snippetbox OWNER TO test_web;
