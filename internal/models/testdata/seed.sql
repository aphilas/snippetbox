INSERT INTO users 
    (name, email, hashed_password)
VALUES
    ('Alice Jones', 'alice@example.com', '$2a$12$NuTjWXm3KKntReFwyBVHyuf/to.HEwTy.eS206TNfkGfr6HzGJSWG');
