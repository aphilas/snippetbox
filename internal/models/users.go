package models

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v5/pgconn"
	"golang.org/x/crypto/bcrypt"
)

const (
	passwordHashCost           = 12
	usersEmailUniqueConstraint = "uq_users_email"
)

type User struct {
	ID             int
	Name           string
	Email          string
	HashedPassword []byte
	Created        time.Time
}

type UserModeler interface {
	Insert(name, email, password string) error
	Authenticate(email, password string) (int, error)
	Exists(id int) (bool, error)
	Get(id int) (*User, error)
	UpdatePassword(id int, currentPassword, newPassword string) error
}

type UserModel struct {
	DB *sql.DB
}

func (m *UserModel) Insert(name, email, password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), passwordHashCost)
	if err != nil {
		return fmt.Errorf("hashing password: %w", err)
	}

	stmt := `INSERT INTO users (name, email, hashed_password) VALUES ($1, $2, $3)`
	_, err = m.DB.Exec(stmt, name, email, string(hashedPassword))
	if err != nil {
		pgErr := new(pgconn.PgError)
		if errors.As(err, &pgErr) {
			if pgErr.Code == pgerrcode.UniqueViolation && strings.Contains(pgErr.Message, usersEmailUniqueConstraint) {
				return ErrDuplicateEmail
			}
		}

		return fmt.Errorf("inserting user: %w", err)
	}

	return nil
}

func (m *UserModel) Authenticate(email, password string) (userID int, err error) {
	stmt := `SELECT id, hashed_password FROM users WHERE email = $1`

	hashedPassword := make([]byte, 0)

	err = m.DB.QueryRow(stmt, email).Scan(&userID, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, ErrInvalidCredentials
		} else {
			return 0, fmt.Errorf("query user: %w", err)
		}
	}

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return 0, ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	return userID, nil
}

func (m *UserModel) Exists(id int) (bool, error) {
	var exists bool

	stmt := `SELECT EXISTS(SELECT 1 FROM users WHERE id = $1) AS "exists"`

	err := m.DB.QueryRow(stmt, id).Scan(&exists)

	return exists, err
}

func (m *UserModel) Get(id int) (*User, error) {
	stmt := `SELECT id, name, email, created FROM users WHERE id = $1`

	user := new(User)
	err := m.DB.QueryRow(stmt, id).Scan(&user.ID, &user.Name, &user.Email, &user.Created)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, ErrNoRecord
	}

	return user, err
}

func (m *UserModel) UpdatePassword(id int, currentPassword, newPassword string) error {
	var (
		userID                string
		currentPasswordHashed []byte
	)

	stmt := `SELECT id, hashed_password FROM users WHERE id = $1`

	err := m.DB.QueryRow(stmt, id).Scan(&userID, &currentPasswordHashed)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return ErrInvalidCredentials
		} else {
			return fmt.Errorf("query user: %w", err)
		}
	}

	err = bcrypt.CompareHashAndPassword(currentPasswordHashed, []byte(currentPassword))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return ErrInvalidCredentials
		} else {
			return err
		}
	}

	newPasswordHashed, err := bcrypt.GenerateFromPassword([]byte(newPassword), passwordHashCost)
	if err != nil {
		return fmt.Errorf("hashing password: %w", err)
	}

	stmt = `UPDATE users SET hashed_password = $2 WHERE id = $1`

	_, err = m.DB.Exec(stmt, id, string(newPasswordHashed))
	if err != nil {
		return fmt.Errorf("updating user: %w", err)
	}

	return nil
}
