# snippetbox

Project code for [Let's Go by Alex Edwards](https://lets-go.alexedwards.net/).

Includes a custom implementation of an in-memory session manager.

## Getting started

```sh
make db

## In a separate shell

# Add env vars
source .env

# Run migrations
docker compose exec -T db psql -U $DB_USER $DB_NAME < ./migrations/ddl.sql
docker compose exec -T db psql -U $DB_USER $DB_NAME < ./migrations/seed.sql

# Set up test db
docker compose exec -T db psql -U $DB_USER $DB_NAME < ./internal/models/testdata/init.sql

# Run the app
make run
```
